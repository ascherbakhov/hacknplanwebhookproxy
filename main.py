# -*- coding: utf-8 -*-
import datetime
import logging
import os
from typing import Optional

import pytz
import requests
from fastapi import FastAPI
from pydantic import BaseModel
from pytz import timezone

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger('webhook')

app = FastAPI()

STAGES = {
    1: 'planned',
    2: 'in progress',
    3: 'testing',
    4: 'completed'
}

class DiscordStageModel(BaseModel):
    Status: str
    Name: Optional[str]
    StageId: int


class UserModel(BaseModel):
    Username: Optional[str]
    Id: int


class DiscordModel(BaseModel):
    content: str
    username: Optional[str]
    avatar_url: Optional[str]
    tts: Optional[bool]


class HacknplanModel(BaseModel):
    Title: str
    UpdateDate: Optional[str]
    CreationDate: Optional[str]
    Stage: DiscordStageModel
    User: UserModel

    def to_message(self):
        return "`Задача '{title}' обновлена в статусе {stage}`". \
            format(title=self.Title, stage=self.get_stage())

    def get_date(self):
        updateDate = self.UpdateDate if self.UpdateDate else self.CreationDate
        utcTime = datetime.datetime.strptime(updateDate, "%Y-%m-%dT%H:%M:%S.%f")
        timezonedTime = pytz.utc.localize(utcTime, is_dst=None).astimezone(timezone('Europe/Moscow'))
        return timezonedTime.strftime("%d-%m-%Y %H:%M:%S %Z%z")

    def get_user(self):
        return self.User.Username if self.User.Username else self.User.Id

    def get_stage(self):
        if self.Stage.Name:
            return self.Stage.Name
        else:
            return STAGES.get(self.Stage.StageId, "не определено")


async def push_nofitifcation(post_data):
    discordToken = os.environ.get('discordToken')
    if discordToken:
        full_url = "https://discordapp.com/api/webhooks/{discord_token}".format(discord_token=discordToken)
        return requests.post(full_url, json=post_data)


@app.post("/discord/{hacknplanToken}")
async def read_hacknplan(hacknplanToken: str, hacknplan: HacknplanModel):
    discordToken = os.environ.get('discordToken')
    awaiting_token = os.environ.get('hacknplanToken')
    if awaiting_token == hacknplanToken:
        content = hacknplan.to_message()
        post_data = DiscordModel(content=content).dict()
        await push_nofitifcation(post_data)
    else:
        logger.error("Hacknplan token {} not exist, awaiting {}, discord {}".format(hacknplanToken, awaiting_token,
                                                                                    discordToken))
