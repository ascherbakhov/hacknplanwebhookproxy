# Install Animal Madness webhook server

FROM tiangolo/uvicorn-gunicorn-fastapi:python3.7
ADD . .
RUN pip3 install -r requirements.txt

EXPOSE 8000

CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000", "--env-file", ".env"]